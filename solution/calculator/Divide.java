package seminar11.solution.calculator;

/**
 * User: macekond
 * Date: 05.05.14
 * Time: 19:28
 */
public class Divide implements IOperation {
    public int calculate(int op1, int op2) {
        return op1 / op2;
    }

    public IOperation getInverse() {
        return new Times();
    }
}
