/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package seminar11.solution.calculator;

import java.util.ArrayDeque;

/**
 *
 * @author pavel
 */

/**
 * Do nasledujiciho kodu doplnte tridy Plus, Minus, Times a Divide.
 * Implementujte metody undo a redo ve tride Invoker.
 * 
*/

class Invoker {
    ArrayDeque<ICommand> commands = new ArrayDeque<ICommand>();
    public void calculate(ICommand c) {
        c.execute();
        commands.push(c);
    }
    
    public void undo() {
        ICommand c = commands.getFirst();
        c.unexecute();
    }
    
    public void redo() {
        ICommand c = commands.getFirst();
        c.execute();
        commands.push(c);
    }
}


public class Client {
    public static void main(String[] args) {
       Calculator c = new Calculator();
       Invoker i = new Invoker();
       i.calculate(new CalculatorCommand(c, new Plus(), 12));
       i.calculate(new CalculatorCommand(c, new Plus(), 10));
       i.calculate(new CalculatorCommand(c, new Minus(), 10));
       i.calculate(new CalculatorCommand(c, new Times(), 10));
       i.calculate(new CalculatorCommand(c, new Divide(), 10));
       System.out.println(c.getValue()); // 12
       i.redo();
       System.out.println(c.getValue()); // 1
       i.undo();
       System.out.println(c.getValue()); // 10
    }
}
