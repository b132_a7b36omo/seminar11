/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package seminar11.solution.calculator;

/**
 *
 * @author pavel
 */
public class CalculatorCommand implements ICommand {
    Calculator calculator;
    IOperation operation;
    int operand;

    public CalculatorCommand(Calculator calc, IOperation op, int operand) {
        calculator = calc;
        this.operation = op;
        this.operand = operand;
    }
    

    public void execute() {
        calculator.doOperation(operation, operand);
    }


    public void unexecute() {
        calculator.doOperation(operation.getInverse(), operand);
    }
    
}
