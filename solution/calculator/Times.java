package seminar11.solution.calculator;

/**
 * User: macekond
 * Date: 05.05.14
 * Time: 19:25
 */
public class Times implements IOperation {
    public int calculate(int op1, int op2) {
        return op1 * op2;
    }

    public IOperation getInverse() {
        return new Divide();
    }
}
