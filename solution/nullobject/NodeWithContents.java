package seminar11.solution.nullobject;

/**
 * User: macekond
 * Date: 05.05.14
 * Time: 18:54
 */
public class NodeWithContents implements Node {
    final int contents;
    Node next;

    NodeWithContents(int contents, Node next) {
        this.contents = contents;
        this.next = next;
    }


    public void add(int value, NodeWithContents previous) {
            next.add(value, this);
    }


    public void addHead(int value, Set set) {
            next.add(value, this);
    }


    public boolean contains(int value) {
        return contents == value || next.contains(value);
    }
}
