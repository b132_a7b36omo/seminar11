package seminar11.solution.nullobject;

/**
 * User: macekond
 * Date: 05.05.14
 * Time: 18:54
 */
public class Set {
    Node head;

    public void add(int value) {
            head.addHead(value, this);
    }

    public boolean contains(int value) {
        return head.contains(value);
    }
}
