package seminar11.solution.nullobject;

/**
 * User: macekond
 * Date: 05.05.14
 * Time: 18:53
 */
public interface Node {
    void add(int value, NodeWithContents previous);

    void addHead(int value, Set set);

    boolean contains(int value);
}