package seminar11.solution.nullobject;

/**
 * User: macekond
 * Date: 05.05.14
 * Time: 18:58
 */
public class NullNode implements Node {
    public void add(int value, NodeWithContents previous) {
         previous.next = new NodeWithContents(value, this);
    }

    public void addHead(int value, Set set) {
        set.head = new NodeWithContents(value, this);
    }

    public boolean contains(int value) {
        return false;
    }
}
