/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package seminar11.calculator;

import java.util.ArrayDeque;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author pavel
 */

/**
 * Do nasledujiciho kodu doplnte tridy Plus, Minus, Times a Divide.
 * Implementujte metody undo a redo ve tride Invoker.
 * 
*/

class Invoker {
    ArrayDeque<ICommand> commands = new ArrayDeque<ICommand>(); 
    public void calculate(ICommand c) {
        c.execute();
        commands.push(c);
    }
    
    public void undo() {
        throw new NotImplementedException();
    }
    
    public void redo() {
        throw new NotImplementedException();
    }
}



public class Client {
    public static void main(String[] args) {
       Calculator c = new Calculator();
       Invoker i = new Invoker();
       i.calculate(new CalculatorCommand(c, new Plus(), 12));
       i.calculate(new CalculatorCommand(c, new Plus(), 10));
       i.calculate(new CalculatorCommand(c, new Minus(), 10));
       i.calculate(new CalculatorCommand(c, new Times(), 10));
       i.calculate(new CalculatorCommand(c, new Divide(), 10));
       System.out.println(c.getValue()); // 12
       i.redo();
       System.out.println(c.getValue()); // 1
       i.undo();
       System.out.println(c.getValue()); // 10
    }
}
