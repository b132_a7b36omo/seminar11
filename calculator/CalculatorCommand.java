/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package seminar11.calculator;

/**
 *
 * @author pavel
 */
public class CalculatorCommand implements ICommand {
    Calculator calculator;
    IOperation operation;
    int operand;
    public CalculatorCommand(Calculator calc, IOperation op, int operand) {
        calculator = calc;
        this.operation = op;
        this.operand = operand;
    }
    
    @Override
    public void execute() {
        calculator.doOperation(operation, operand);
    }

    @Override
    public void unexecute() {
        calculator.doOperation(operation.getInverse(), operand);
    }
    
}
