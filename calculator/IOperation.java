/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package seminar11.calculator;

/**
 *
 * @author pavel
 */
public interface IOperation {
    public int calculate(int op1, int op2);
    public IOperation getInverse();
}

