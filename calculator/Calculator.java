/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package seminar11.calculator;

/**
 *
 * @author pavel
 */
public class Calculator {
    int current_value = 0;
    public void doOperation(IOperation op, int operand) {
        current_value = op.calculate(current_value, operand);
    }
    
    public int getValue() {
        return current_value;
    }
}
