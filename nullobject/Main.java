/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package seminar11.nullobject;

/**
 *
 * @author pavel
 */

/**
 * Nasledujici kod upravte s pouzitim navrhoveho vzoru NullObject (odstrante testy na null).
 */

class Set {
    Node head;

    void add(int value) {
        if(head==null) {
            head = new NodeWithContents(value, head);
        } else {
            head.addHead(value, this);
        }
    }

    boolean contains(int value) {
        return head.contains(value);
    }
}

interface Node {
    void add(int value, NodeWithContents previous);

    void addHead(int value, Set set);

    boolean contains(int value);
}

class NodeWithContents implements Node {
    final int contents;
    Node next;

    NodeWithContents(int contents, Node next) {
        this.contents = contents;
        this.next = next;
    }

    @Override
    public void add(int value, NodeWithContents previous) {
        if(next!=null) {
            next.add(value, this);
        } else {
            next = new NodeWithContents(value, null);
        }
        
    }

    @Override
    public void addHead(int value, Set set) {
        if(next!=null) {
            next.add(value, this);
        } else {
            next = new NodeWithContents(value, null);
        }
    }

    @Override
    public boolean contains(int value) {
        if(next==null) return false;
        return contents == value || next.contains(value);
    }
}

public class Main{
    public static void main(String [] args){
        Set s = new Set();
        s.add(10);
        s.add(20);
        
    }
}
