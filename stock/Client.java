/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package seminar11.stock;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pavel
 */

//ConcreteCommand Class.
class BuyStockOrder implements IOrder {
    private StockTrade stock;
    public BuyStockOrder ( StockTrade st) {
        stock = st;
    }
    @Override
    public void execute( ) {
        stock.buy();
    }
}

//ConcreteCommand Class.
class SellStockOrder implements IOrder {
    private StockTrade stock;
    public SellStockOrder ( StockTrade st) {
        stock = st;
    }
    @Override
    public void execute( ) {
        stock.sell();
    }
}

// Client
public class Client {
    public static void main(String[] args) {
        StockTrade stock = new StockTrade();
        BuyStockOrder bsc = new BuyStockOrder (stock);
        SellStockOrder ssc = new SellStockOrder (stock);
        Agent agent = new Agent();
        agent.openStockMarket();
        agent.placeOrder(bsc); // Buy Shares
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        agent.placeOrder(ssc); // Sell Shares
    }
}

//public class Client {
    
//}
