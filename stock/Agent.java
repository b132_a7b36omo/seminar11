/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package seminar11.stock;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pavel
 */


// Invoker.
public class Agent {
    private LinkedBlockingQueue<IOrder> ordersQueue = new LinkedBlockingQueue<IOrder>();
    Thread t;
    final boolean terminate = false;
    public Agent() {
        
    }
    
    void placeOrder(IOrder order) {
        System.out.println("Placing Order");
        ordersQueue.add(order);
       // order.execute(ordersQueue.poll());
    }
    
    void openStockMarket() {
        System.out.println("Opening Stock Market");
//        try {
            t = (new Thread() {
              @Override
              public void run() {
                 while(true) {
                        try {
                            ordersQueue.take().execute();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Agent.class.getName()).log(Level.SEVERE, null, ex);
                        }
                 }
              }
            });
            t.start();
//            t.join();
//        } catch (InterruptedException ex) {
//            Logger.getLogger(Agent.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
}
